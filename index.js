const express = require('express');
const app = express();

app.get('/', function( req, res, next ){
  res.status(418);
  res.sendFile(__dirname + '/html/coffee.html');
});

app.get('/*', function( req, res, next ){
  res.redirect('/');
});

app.listen(3338);
